var mySwiper = new Swiper ('.swiper-container', {
    autoplay:{
        delay: 5000,
    },
    // Optional parameters
    direction: 'horizontal',
    loop: true,
    speed: 600,
    // Navigation arrows
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  })