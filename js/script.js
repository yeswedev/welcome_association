"use strict";

document.addEventListener('DOMContentLoaded', function(){

    // toggle class open pour le burger et le menu et affichage du burger

    var burgerElt = document.getElementById('burger');
    var menuElt = document.getElementById('menu');

        //au chargement de la page

    if (document.scrollingElement.clientWidth <= 768) {
        burgerElt.style.display = "block";
    }
        //au resize
    window.addEventListener('resize', function(){
        if (document.scrollingElement.clientWidth <= 768) {
            burgerElt.style.display = "block";
        } if (document.scrollingElement.clientWidth > 768){
            burgerElt.style.display = "none";
        }
    })
        //au scroll
    window.addEventListener('scroll', function(){
        burgerElt.classList.remove('is-open');
        menuElt.classList.remove('is-open');
    })
        //au click
    burgerElt.addEventListener('click', function(){
        this.classList.toggle('is-open');
        menuElt.classList.toggle('is-open');
    })
})

// Ajout de Quentin bouton permettant d'afficher plus d'images
function displayMoreImages(element){
    var blocTeam = document.getElementsByClassName('blocTeam');
    for(var i = 0; i < blocTeam.length; i++) {
        blocTeam[i].style.display = "flex";
    }

    element.style.display = "none";
} 

