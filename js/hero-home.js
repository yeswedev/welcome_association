// animation scroll bouton
var btnElt = document.getElementById('descendre');
var valeursElt = document.getElementById('nos-valeurs');
var navElts = document.getElementById('menu').getElementsByTagName('li');
var animationScroll = null;

function btnScroll(){
    var scrollTop = valeursElt.offsetTop;
    if (window.scrollY < scrollTop && ((window.scrollY+document.scrollingElement.clientHeight) < document.scrollingElement.offsetHeight)) {
        window.scroll(0, window.scrollY+15);
        animationScroll = requestAnimationFrame(btnScroll);
    } else {
        cancelAnimationFrame(animationScroll);
    }
}
document.addEventListener('DOMContentLoaded', function(){
    btnElt.addEventListener('click', function(e){
        e.preventDefault();
        animationScroll = requestAnimationFrame(btnScroll);
    });
    navElts[0].addEventListener('click', function(e){
        e.preventDefault();
        animationScroll = requestAnimationFrame(btnScroll);
    });
})