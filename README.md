# welcome_association project

## installation des dépendances

Installer composer > https://doc.ubuntu-fr.org/composer

Installer la dépendance Blade

```
composer install
```

## Compilation des SASS

Coucou, pour lancer la compilation en continu :

```
while true; do sleep 3; ./scss2css.sh scss/main.scss css/main.css ; done
```
La méthode vous demandera d'installer des packets via sudo apt get


## Pour lancer le serveur PHP afin de voir ce magnifique site

Aller dans un nouveau terminal (Ctrl + MAJ + N dans un terminal)

```
cd CHEMIN-VERS-MON-PROJET/welcome_association
php -S 127.0.0.1:8001
```