@extends('layouts.app')

@section('content')

    <h1>Ceci est un H1</h1>
    <p>
        Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado
        desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um
        livro
        de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração
        eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou
        decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de
        editoração eletrônica como Aldus PageMaker.
    </p>
    <h2>Ceci est un H2</h2>
    <p>
        Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado
        desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um
        livro
        de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração
        eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou
        decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de
        editoração eletrônica como Aldus PageMaker.
    </p>
    <h3>Ceci est un H3</h3>
    <p>
        Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado
        desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um
        livro
        de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração
        eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou
        decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de
        editoração eletrônica como Aldus PageMaker.
    </p>
    <h4>Ceci est un H4</h4>
    <p>
        Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado
        desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um
        livro
        de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração
        eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou
        decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de
        editoração eletrônica como Aldus PageMaker.
    </p>

@endsection