@extends('layouts.app')

@section('more-css')
    {{-- Swiper slider --}}
    <link rel="stylesheet" href="vendors/swiper/swiper.min.css" type="text/css">
@endsection

@section('content')

    <section class="swiper-about">
        <div class="swiper-container">
            <div class="swiper-wrapper swiper-container__wrapper">
                <div class="swiper-slide" style="background: url('../img/projet_1.jpg') center top / cover">
                    <div class="container">
                        <div class="project-description">
                            <h2 class="project-description__title">Nom du projet</h2>
                            <p class="project-description__details">Technologie</p>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide" style="background: url('../img/projet_2.jpg') center top / cover">
                    <div class="container">
                        <div class="project-description">
                            <h2 class="project-description__title">Nom du projet</h2>
                            <p class="project-description__details">Technologie</p>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide" style="background: url('../img/projet_3.jpg') center top / cover">
                    <div class="container">
                        <div class="project-description">
                            <h2 class="project-description__title">Nom du projet</h2>
                            <p class="project-description__details">Technologie</p>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide" style="background: url('../img/projet_4.jpg') center top / cover">
                    <div class="container">
                        <div class="project-description">
                            <h2 class="project-description__title">Nom du projet</h2>
                            <p class="project-description__details">Technologie</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container swiper-container__btn-container">
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>
            </div>
        </div>
    </section>

    <section class="first-section">
        <h2>Notre process</h2>
        <div class="process">
            <article>
                <h3>Conseil</h3>
                <p>Selon votre besoin, nous optons pour la meilleure solution technique.</p>
            </article>
            <article>
                <h3>Livrables</h3>
                <p>Nos développeurs réalisent les travaux confiés par sprints successifs.</p>
            </article>
            <article>
                <h3>Vérifications</h3>
                <p>Vous testez et validez en continu pour éviter les mauvaises surprises.</p>
            </article>
            <article>
                <h3>Livraison</h3>
                <p>Le projet vous est livré, dans le respect du délai, du budget.</p>
            </article>
        </div>
    </section>

    <section class="notreEquipe">
        <h2>Notre Equipe</h2>
        <div class="bloc_Flex_Team">
            <figure class="blocTeam">
                <img class="blocTeam__img" src="img/chats/chat1.png" alt="">
                <figcaption class="blocTeam__caption">Nom Prenom</figcaption>
                <figcaption class="blocTeam__caption--text-sm">Fonction</figcaption>
            </figure>
            <figure class="blocTeam">
                <img class="blocTeam__img" src="img/chats/chat2.png" alt="">
                <figcaption class="blocTeam__caption">Nom Prenom</figcaption>
                <figcaption class="blocTeam__caption--text-sm">Fonction</figcaption>
            </figure>
            <figure class="blocTeam">
                <img class="blocTeam__img" src="img/chats/chat3.png" alt="">
               <figcaption class="blocTeam__caption">Nom Prenom</figcaption>
                <figcaption class="blocTeam__caption--text-sm">Fonction</figcaption>
            </figure>
            <figure class="blocTeam">
                <img class="blocTeam__img" src="img/chats/chat4.png" alt="">
               <figcaption class="blocTeam__caption">Nom Prenom</figcaption>
                <figcaption class="blocTeam__caption--text-sm">Fonction</figcaption>
            </figure>
            <figure class="blocTeam">
                <img class="blocTeam__img" src="img/chats/chat5.png" alt="">
               <figcaption class="blocTeam__caption">Nom Prenom</figcaption>
                <figcaption class="blocTeam__caption--text-sm">Fonction</figcaption>
            </figure>
            <figure class="blocTeam" >
                <img class="blocTeam__img" src="img/chats/chat6.png" alt="">
              <figcaption class="blocTeam__caption">Nom Prenom</figcaption>
                <figcaption class="blocTeam__caption--text-sm">Fonction</figcaption>
            </figure>
            
        
           <figure class="blocTeam">
                <img class="blocTeam__img" src="img/chats/chat7.png" alt="">
                <figcaption class="blocTeam__caption">Nom Prenom</figcaption>
                <figcaption class="blocTeam__caption--text-sm">Fonction</figcaption>
            </figure>
           <figure class="blocTeam">
                <img class="blocTeam__img" src="img/chats/chat8.png" alt="">
                <figcaption class="blocTeam__caption">Nom Prenom</figcaption>
                <figcaption class="blocTeam__caption--text-sm">Fonction</figcaption>
            </figure>
           <figure class="blocTeam">
                <img class="blocTeam__img" src="img/chats/chat9.png" alt="">
                <figcaption class="blocTeam__caption">Nom Prenom</figcaption>
                <figcaption class="blocTeam__caption--text-sm">Fonction</figcaption>
            </figure>
            <figure class="blocTeam">
                <img class="blocTeam__img" src="img/chats/chat10.png" alt="">
                <figcaption class="blocTeam__caption">Nom Prenom</figcaption>
                <figcaption class="blocTeam__caption--text-sm">Fonction</figcaption>
            </figure>
            <figure class="blocTeam">
                <img class="blocTeam__img" src="img/chats/chat11.png" alt="">
                <figcaption class="blocTeam__caption">Nom Prenom</figcaption>
                <figcaption class="blocTeam__caption--text-sm">Fonction</figcaption>
            </figure>
           <figure class="blocTeam">
                <img class="blocTeam__img" src="img/chats/chat12.png" alt="">
                <figcaption class="blocTeam__caption"> Nom Prenom</figcaption>
                <figcaption class="blocTeam__caption--text-sm">Fonction</figcaption>
            </figure>
            <figure class="blocTeam">
                <img class="blocTeam__img" src="img/chats/chat13.png" alt="">
                <figcaption class="blocTeam__caption">Nom Prenom</figcaption>
                <figcaption class="blocTeam__caption--text-sm">Fonction</figcaption>
            </figure>
            <figure class="blocTeam">
                <img class="blocTeam__img" src="img/chats/chat14.png" alt="">
                <figcaption class="blocTeam__caption">Nom Prenom</figcaption>
                <figcaption class="blocTeam__caption--text-sm">Fonction</figcaption>
            </figure>
            <figure class="blocTeam">
                <img class="blocTeam__img" src="img/chats/chat15.png" alt="">
                <figcaption class="blocTeam__caption">Nom Prenom</figcaption>
                <figcaption class="blocTeam__caption--text-sm">Fonction</figcaption>
            </figure>
            <figure class="blocTeam">
                <img class="blocTeam__img" src="img/chats/chat16.png" alt="">
                <figcaption class="blocTeam__caption">Nom Prenom</figcaption>
                <figcaption class="blocTeam__caption--text-sm">Fonction</figcaption>
            </figure>
            <figure class="blocTeam">
                <img class="blocTeam__img" src="img/chats/chat17.png" alt="">
                <figcaption class="blocTeam__caption">Nom Prenom</figcaption>
                <figcaption class="blocTeam__caption--text-sm">Fonction</figcaption>
            </figure>
            <figure class="blocTeam">
                <img class="blocTeam__img" src="img/chats/chat18.png" alt="">
                <figcaption class="blocTeam__caption">Nom Prenom</figcaption>
                <figcaption class="blocTeam__caption--text-sm">Fonction</figcaption>
            </figure>


        </div>
        <!-- boutton: -->
            <div class="btn_more" onclick="displayMoreImages(this);">
                <div class="btn_more__background">
                    <span>Voir plus</span>
                </div>
            </div>
    </section>

    <section id="carte">
        <h2>Nous trouver</h2>
        <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=-1.7249178886413576%2C48.102050720176024%2C-1.711474657058716%2C48.108469962364666&amp;layer=mapnik&amp;marker=48.10526044148294%2C-1.7182016372680664" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=48.10526&amp;mlon=-1.71820#map=17/48.10526/-1.71820"></a></small>
    </section>

@endsection

@section('more-js')
    {{-- Swiper Slider --}}
    <script src="vendors/swiper/swiper.min.js"></script>
    {{-- About Slider --}}
    <script src="js/slider-about.js"></script>
@endsection