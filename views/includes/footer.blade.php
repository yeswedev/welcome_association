<footer class="footer">
    <div class="btn_contact">
        <div class="btn_contact__bg">
            <a href="#">Prendre contact</a>
        </div>
    </div>

    <span class="hr--footer"></span>

    <small>
        <p>
            Yes We Dev est une marque appartenant à OUR REAL WINS SAS, immatriculée au RCS de Rennes sous le numéro RCS 801 172 776, ayant son siège social au 8 quai Robinot de St Cyr 35000 RENNES.
        </p>
        <div class="love-bzh">
            <span>Fait avec</span>
            <img src="img/footer/heart.png" alt="pictogramme d'un coeur rouge">
            <span>en BZH</span>
        </div>
        <a href="mentions.php">Mentions légales</a>
    </small>
</footer>