<head>
    <meta charset="UTF-8">
    <title>Yes We Dev</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcup icon" type="image/png" href="img/pictoywd.png">
    @yield('more-css')
    <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900&amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="css/main.css" type="text/css">
</head>