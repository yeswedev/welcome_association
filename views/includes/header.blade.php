<header>
    <div class="container">
        <nav class="header_nav">
            <a href="/">
                <img src="img/logoywd.png" alt="Yes We Dev" title="Yes We Dev">
            </a>
            <div class="navigation">
                <div id="burger" class="navigation__burger">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <ul id="menu" class="menu">
                    <li class="menu__li"><a href="/#nos-valeurs">nos valeurs</a></li>
                    <li class="menu__li"><a href="about.php">qui sommes nous</a></li>
                    <li class="menu__li"><a href="contact.php">contact</a></li>
                </ul>
            </div>
        </nav>
    </div>
</header>