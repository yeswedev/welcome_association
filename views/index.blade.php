@extends('layouts.app')

@section('more-css')
@endsection

@section('content')

    <section class="hero">
        <div class="hero__container container">
            <h1>We are web artisans !</h1>
            <a class="btn_scroll" id="test" href="#proposition"><img src="img/callto.png" alt="Descendre" title="Descendre" id="descendre"></a>
        </div>
    </section>

    <section class="container first-section" id="nos-valeurs">
        <h2>Nos valeurs</h2>
        <section class="text-icon-contenant">
            <article class="text-icon">
                <i class="icon-efficient"></i>
                <h3 class="text-icon__title">Éfficacité</h3>
                <p class="text-icon__content">Mise en place d’un process récurrent (CMS, contenus) pour réduire le temps perdu</p>
            </article>
            <article class="text-icon">
                <i class="icon-collaboration"></i>
                <h3 class="text-icon__title">Collaboration saine</h3>
                <p class="text-icon__content">Nous facturons avec transparence et livrons vos projets en un temps record</p>
            </article>
            <article class="text-icon">
                <i class="icon-quality"></i>
                <h3 class="text-icon__title">Qualité technique</h3>
                <p class="text-icon__content">Notre équipe réalise des projets légers, SEO & responsive friendly</p>
            </article>
        </section>
        <hr class="hr">
    </section>

    <section class="container second-section" id="proposition">
        <h2>Nous vous proposons</h2>
        <div class="text-top-proposition">
            <h3 class="text-top-proposition__text">Une réalisation à moindre coût pour votre structure associative en <span class="text-top-proposition__text--span">4 étapes</span></h3>
        </div>
        <div class="text-icon-contenant">
            <div  class="text-icon--width-sm">
                <h3 class="text-icon__title">Conseil</h3>
                <i class="icon-advice"></i>
            </div>
            <div class="text-icon--width-sm">
                <h3 class="text-icon__title" >Accompagnement</h3>
                <i class="icon-lead"></i>
            </div>
            <div class="text-icon--width-sm">
                <h3 class="text-icon__title">Réalisation</h3>
                <i class="icon-coding"></i>
            </div>
            <div class="text-icon--width-sm">
                <h3 class="text-icon__title">Livraison</h3>
                <i class="icon-delivery"></i>
            </div>
        </div>
    </section>

@endsection

@section('more-js')
    {{-- Button Scroll --}}
    <script src="js/hero-home.js"></script>
@endsection