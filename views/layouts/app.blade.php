<!DOCTYPE html>
<html lang="fr">

@include('includes.head')
<body>
@include('includes.header')

<main class="BodyLayout">
    @yield('content')
</main>


@include('includes.footer')
@yield('more-js')
@include('includes.javascript')
</body>

</html>
